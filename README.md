# forum-front

> case estudy for esfinge gamification project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Nuxt -> framework do vue, vue é um framework e Nuxt é um framework que usa o vue (roubado).

Vuex e localStorage ou sessionStorage (persistir dados na sessionStorage).
